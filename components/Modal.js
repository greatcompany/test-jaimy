import Link from 'next/link';

const Modal = ({country, closeHandler}) => (
  <div className='modalContainer'>
	<h1>{country.name}</h1>
	<h2>Capital: {country.capital}</h2>

	<table>
	  <tbody>
	  <tr>
		<td>Alpha 2 Code</td>
		<td>{country.alpha2Code}</td>
	  </tr>
	  <tr>
		<td>Region</td>
		<td>{country.region}</td>
	  </tr>
	  <tr>
		<td>Population</td>
		<td>{country.population}</td>
	  </tr>
	  </tbody>

	</table>
	<button onClick={() => closeHandler()}>Close</button>
  </div>
);

export default Modal;
