import NavButton from './NavButton'
import Link from 'next/link';
import React from 'react'
const Header = () => (
    <header>
	  <NavButton href="/" label={"All Countries"} />
	  <NavButton href="/region" label={"Region"} />
	  <NavButton href="/Search" label={"Search"} />
    </header>
);

export default Header;
