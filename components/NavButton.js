import Link from 'next/link';
import { useRouter } from 'next/router'

const NavButton = ({href, label}) => {
  return (
	<Link href={href}>
	  <a className={"nav_button"}>{label}</a>
	</Link>
  );
}

export default NavButton;
