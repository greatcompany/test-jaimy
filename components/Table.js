import Modal from './Modal'
import Link from 'next/link';
import {useState} from "react";

import PropTypes from 'prop-types'

const Table = ({ countries, numberOfRowsPerPage } ) => {
    const [page, setPage] = useState(0);
    const [selectedCountry, setSelectedCountry] = useState(undefined);

    const paginatedCountries = countries.slice(page * numberOfRowsPerPage, page * numberOfRowsPerPage + numberOfRowsPerPage)

    const rows =  paginatedCountries.map(country => <tr onClick={() => setSelectedCountry(country)}>
        <td className={'countryContentFirst'}>{country.name}</td>
        <td className={'countryContent'}>{country.capital}</td>
        <td className={'countryContent'}>{country.alpha2Code}</td>
    </tr>)

    return (
        <div>
            <div className={'container'}>
        <table>
            <thead>
            <tr>
                <td>Name</td>
                <td>Capital</td>
                <td>Alpha2Code</td>
            </tr>
            </thead>
            <tbody>
            {rows}
            </tbody>
        </table>
            </div>
            <div className={'container'}>
            <div className='pagination'>
                <button disabled={page === 0} onClick={() => setPage(prevValue => prevValue - 1)}>&lt;</button>
			  	<p className={'pageCount'}>Page: {page + 1} / {Math.ceil(countries.length / numberOfRowsPerPage)}</p>
                <button disabled={((page + 1 ) * numberOfRowsPerPage > countries.length) || page === 24} onClick={() => setPage(prevValue => prevValue + 1)}>&gt;</button>
            </div>
            {selectedCountry !== undefined &&

				  <Modal country={selectedCountry} closeHandler={() => setSelectedCountry(undefined)}/>

            }
            </div>
        </div>
    );
}

Table.propTypes = {
    numberOfRowsPerPage: PropTypes.number
}

Table.defaultProps = {
    numberOfRowsPerPage: 10,
}

export default Table;
