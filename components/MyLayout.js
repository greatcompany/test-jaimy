import Header from './Header';

const Layout = props => (
    <div className={"layoutContainer"}>
        <Header />
        {props.children}
    </div>
);

export default Layout;
