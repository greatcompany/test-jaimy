// This page has defined `getInitialProps` to do data fetching.
// Next.js will execute `getInitialProps`
// It will wait for the result of `getInitialProps`
// When the results comes back Next.js will render the page.
// Next.js will do this for every request that comes in.
import fetch from 'isomorphic-unfetch'
import Table from "../components/Table";
import Header from '../components/Header'
import Layout from "../components/MyLayout";

function Region({ countries }) {

    console.log(countries)


    const filtered = countries.filter(country => {
        if(country && country.regionalBlocs) {
            return country.regionalBlocs.filter(regionalBlock => regionalBlock.name === 'European Union').length > 0
        }
        return false
    })

    console.log("Filtered", filtered)

    return <Layout>
        <h1>Europenion Union only:</h1>
        <Table countries={filtered}/>
        <style jsx>{`
        h1,
        a, {
          font-family: 'Arial';
        }
      `}</style>
    </Layout>
}

Region.getInitialProps = async () => {
    const res = await fetch('https://restcountries.eu/rest/v2/all')
    const json = await res.json()
    console.log(`Show data fetched: ${json}`);

    return {
        countries: json
    };
}

Region.defaultProps = {
    countries:[]
}

export default Region