// This page has defined `getInitialProps` to do data fetching.
// Next.js will execute `getInitialProps`
// It will wait for the result of `getInitialProps`
// When the results comes back Next.js will render the page.
// Next.js will do this for every request that comes in.
import fetch from 'isomorphic-unfetch'
import Table from "../components/Table";
import Header from '../components/Header'
import React, {useState} from "react";
import Layout from "../components/MyLayout";



function Search({ countries }) {

    const [searchTerm, setSearchTerm] = useState("");
    const handleChange = event => {
        setSearchTerm(event.target.value);
    };

    const filtered = countries.filter(country => country.name.toLowerCase().includes(searchTerm))

    return <Layout>
        <input
            type="text"
            placeholder="Search"
            value={searchTerm}
            onChange={handleChange}
        />

            <Table countries={filtered}/>


        </Layout>

}

Search.getInitialProps = async () => {
    const res = await fetch('https://restcountries.eu/rest/v2/all')
    const json = await res.json()

    return {
        countries: json
    };
}

Search.defaultProps = {
    countries:[]
}

export default Search