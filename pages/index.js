// This page has defined `getInitialProps` to do data fetching.
// Next.js will execute `getInitialProps`
// It will wait for the result of `getInitialProps`
// When the results comes back Next.js will render the page.
// Next.js will do this for every request that comes in.
import fetch from 'isomorphic-unfetch'
import Table from "../components/Table";
import Layout from '../components/MyLayout'


function Index({ countries }) {

    console.log(countries)


    return ( <Layout>

        <h1>All Countries:</h1>
        <Table  countries={countries}/>

        </Layout> );
}

Index.getInitialProps = async () => {
    const res = await fetch('https://restcountries.eu/rest/v2/all')
    const json = await res.json()
    console.log(`Show data fetched: ${json}`);

    return {
        countries: json
    };
}

Index.defaultProps = {
    countries:[]
}

export default Index